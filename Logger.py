import json
import pandas as pd
import requests as r
from sqlalchemy import create_engine
import pymysql

#get most recent refresh token
with open("refreshTokens.txt","r") as file:
    for refresh_token in file:
        pass
print(refresh_token)

#get access token

refresh_url = f'https://www.strava.com/oauth/token?client_id=75809&client_secret=3dbfb14d30e889025117457203eda3a6ca527f04&refresh_token={refresh_token}&grant_type=refresh_token'
refresh_req = r.post(refresh_url)
access_token = refresh_req.json()['access_token']
new_refresh_token = str(refresh_req.json()['refresh_token'])


#append new refresh token to text file
with open("refreshTokens.txt","a") as appendFile:
    appendFile.write('\n'+new_refresh_token)


#get api data
var_headers = {'Authorization':f'Bearer {access_token}'}

url = f'https://www.strava.com/api/v3/athlete/activities?page=1&per_page=200'
req = r.get(url,headers=var_headers)
j = req.json()
df = pd.json_normalize(j)
#if page one is full, start looping
if len(j) <= 200:
    print(f'Page one at {len(j)}, starting looping')
    pg = 2

    continue_looping = True
else:
    print('Page one not full, no loop')
    continue_looping = False

while continue_looping:
    print(f'loading page {pg}')
    url = f'https://www.strava.com/api/v3/athlete/activities?page={pg}&per_page=200'
    req = r.get(url,headers=var_headers)
    j = req.json()
    df = df.append(pd.json_normalize(j))
    pg = pg + 1
    if len(j) == 0:
        continue_looping = False



#lets get a connection to our SQL server
sqlEngine = create_engine('mysql+pymysql://datastudio:n7v6eShs.k23@212.71.248.36/stravalog', pool_recycle=3600)
dbConnection = sqlEngine.connect()

#df['year'] = pd.DatetimeIndex(df['start_date']).year
#df['month'] = pd.DatetimeIndex(df['start_date']).month
df['lastUpdatedDateTime'] = pd.Timestamp.now()

#df.to_csv('C:\\Users\\GJones\\Downloads\\ShartInMyPants.csv',index=False)

forSql = df[['start_date','total_elevation_gain','lastUpdatedDateTime']]
forSql.to_sql('staging',dbConnection,if_exists='replace',index=False)
